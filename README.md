# SimpleRoleplay

## Main Contributors
* MrNeta ([GitHub](https://github.com/MrNeta) | [GT-MP Forums](https://gt-mp.net/user/745-neta/))
* DurtyFree ([GitHub](https://github.com/durtyfree) | [GT-MP Forums](https://gt-mp.net/user/2-durtyfree/))

## Credits
* [Custom Character Creator](https://gt-mp.net/forum/thread/1299-custom-character-creator/) by [rootcause](https://gt-mp.net/user/1881-rootcause/)
* Doormanager by [Guadmaz](https://github.com/Guad/)
* [Compass](https://github.com/3marproof/Compass) by [3marproof](https://github.com/3marproof)
* [Synced Walking Styles](https://gt-mp.net/forum/thread/1230-synced-walking-styles/) by [rootcause](https://gt-mp.net/user/1881-rootcause/)
* [Synced Moods](https://gt-mp.net/forum/thread/1321-synced-moods/) by [rootcause](https://gt-mp.net/user/1881-rootcause/)

## Features
* Vehicle System
* Faction System
* Multiple Characters possible